import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material/material.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MaterialTableComponent } from './Mycomponents/material-table/material-table.component';
import { AddUserFormComponent } from './Mycomponents/add-user-form/add-user-form.component';
import { FormsModule } from '@angular/forms';
import { ConfirmDeleteComponent } from './Mycomponents/confirm-delete/confirm-delete.component';

@NgModule({
  declarations: [AppComponent, MaterialTableComponent, AddUserFormComponent, ConfirmDeleteComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
