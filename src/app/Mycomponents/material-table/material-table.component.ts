import { SnackBarServiceService } from './../../services/snack-bar-service.service';
import { UpdateTableService } from './../../services/update-table.service';
import { DeleteSingleUserService } from './../../services/delete-single-user.service';
import { EditNotificationService } from './../../services/edit-notification.service';
import { GetUserService } from './../../services/get-user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatDialog } from '@angular/material/dialog';
import { AddUserFormComponent } from '../add-user-form/add-user-form.component';
import { ConfirmDialogService } from 'src/app/services/confirm-dialog.service';
export interface User {
  id: number;
  name: string;
  email: string;
  mob: number;
}

let USER_DATA: User[] = [];

@Component({
  selector: 'app-material-table',
  templateUrl: './material-table.component.html',
  styleUrls: ['./material-table.component.css'],
})
export class MaterialTableComponent implements OnInit {
  dataSource;
  displayedColumns: string[] = ['id', 'name', 'email', 'mob', 'action'];
  search: string = '';

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private user: GetUserService,
    private dialog: MatDialog,
    private edit: EditNotificationService,
    private del: DeleteSingleUserService,
    private notify: UpdateTableService,
    private confirm: ConfirmDialogService,
    private snack: SnackBarServiceService
  ) {}

  ngOnInit(): void {
    this.getAllUser();
    this.notify.$tableUpdate.subscribe((id) => {
      this.getAllUser();
    });
  }

  deleteData(row) {
    // console.log('inside deleteData row');
    // this.del.deleteUser(row.id).subscribe((msg) => {
    //   this.getAllUser();
    // });
    this.confirm
      .confirmOperation()
      .afterClosed()
      .subscribe((res) => {
        if (res) {
          this.del.deleteUser(row.id).subscribe((msg) => {
            this.getAllUser();
            this.snack.deleted('deleted successfully !');
          });
        }
      });
  }

  getAllUser() {
    this.user.getUsers().subscribe((allusers: User[]) => {
      USER_DATA = allusers;
      this.dataSource = new MatTableDataSource(USER_DATA);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });
  }

  editData(row) {
    this.edit.updateNotification(row.id);
    this.dialog.open(AddUserFormComponent, { width: '40%' });
  }

  applyFilter(input: string) {
    this.dataSource.filter = input.trim().toLowerCase();
  }

  onAdd() {
    this.dialog.open(AddUserFormComponent, {
      width: '40%',
      disableClose: true,
    });
  }

  onClear() {
    this.search = '';
    this.dataSource.filter = this.search.trim().toLowerCase();
  }
}
