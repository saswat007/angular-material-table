import { UpdateTableService } from './../../services/update-table.service';
import { GetSingleUserService } from './../../services/get-single-user.service';
import { EditNotificationService } from './../../services/edit-notification.service';
import { MatDialogRef } from '@angular/material/dialog';
import { SnackBarServiceService } from './../../services/snack-bar-service.service';
import { AddUserService } from './../../services/add-user.service';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-user-form',
  templateUrl: './add-user-form.component.html',
  styleUrls: ['./add-user-form.component.css'],
})
export class AddUserFormComponent implements OnInit, AfterViewInit {
  userModel: any = {};
  switch = false;
  addUserSubscription: Subscription;
  @ViewChild('userForm') userForm: NgForm;
  updatedUser: any = {};

  constructor(
    private add: AddUserService,
    private snack: SnackBarServiceService,
    private dialogRef: MatDialogRef<AddUserFormComponent>,
    private edit: EditNotificationService,
    private getOne: GetSingleUserService,
    private notify: UpdateTableService
  ) {}

  ngOnInit(): void {
    console.log('visible');
    this.addUserSubscription = this.edit.$editUser.subscribe((id: number) => {
      // console.log('id received is ' + id);
      this.updateFields(id);
      this.addUserSubscription.unsubscribe();
    });
  }

  ngAfterViewInit() {
    // console.log('after view method');
  }

  newUser(userForm) {
    this.add.addUser(this.userModel).subscribe((user) => {
      // console.log("success");
      // console.log(user);
      this.switch = true;
      this.notify.updateTable(this.userModel.id);
    });
    // console.log(this.userModel);
    this.onClose(userForm);
    this.snack.success('submitted successfully !');
  }
  onClose(userForm) {
    userForm.resetForm();
    this.dialogRef.close();
  }

  updateFields(id) {
    let usernew: any = {};
    this.getOne.getSingleUser(id).subscribe((user: any) => {
      usernew = user;
    });
    setTimeout(() => {
      this.userForm.form.setValue({
        id: usernew.id,
        name: usernew.name,
        email: usernew.email,
        mob: usernew.mob,
      });

      // this.userModel.id = user.id;
      // this.userModel.name = user.name;
      // this.userModel.email = user.email;
      // this.userModel.mob = user.mob;

      // console.log(this.updatedUser);
      // console.log(this.userModel);
      this.switch = true;
      console.log(this.switch);
    });
  }
}
