import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EditNotificationService {

  private _editSource = new Subject<number>();
  $editUser = this._editSource.asObservable();

  constructor() { }

  updateNotification(id: number){
    this._editSource.next(id);
  }
}
