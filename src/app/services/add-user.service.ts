import { userUrl } from './../../../config/api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AddUserService {
  constructor(private http: HttpClient) {}

  addUser(user) {
    return this.http.post(userUrl, user);
  }
}
