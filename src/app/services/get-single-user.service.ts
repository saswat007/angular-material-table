import { userUrl } from './../../../config/api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class GetSingleUserService {
  constructor(private http: HttpClient) {}

  getSingleUser(id: number) {
    return this.http.get(userUrl + id);
  }
}
