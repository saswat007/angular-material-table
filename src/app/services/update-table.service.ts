import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UpdateTableService {

  private _update = new Subject<number>();
  $tableUpdate = this._update.asObservable();

  constructor() { }

  updateTable(id:number){
    this._update.next(id);
  }
}
