import { ConfirmDeleteComponent } from './../Mycomponents/confirm-delete/confirm-delete.component';
import { MatDialog } from '@angular/material/dialog';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ConfirmDialogService {
  constructor(private dialog: MatDialog) {}

  confirmOperation() {
    return this.dialog.open(ConfirmDeleteComponent, {
      width: '20%',
      height: '20%',
      panelClass: 'confirm-dialog-container',
    });
  }
}
