import { userUrl } from './../../../config/api';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DeleteSingleUserService {

  constructor(private http: HttpClient) { }

  deleteUser(id: number){
   // console.log('inside delete user service');
    return this.http.delete(userUrl + id);
  }
}
