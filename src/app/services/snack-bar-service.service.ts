import { MatSnackBar } from '@angular/material/snack-bar';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SnackBarServiceService {
  constructor(private snackBar: MatSnackBar) {}

  success(msg) {
    this.snackBar.open(msg, 'ok', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: 'Snackbar',
    });
  }
  deleted(msg) {
    this.snackBar.open(msg, 'ok', {
      duration: 5000,
      horizontalPosition: 'center',
      verticalPosition: 'top',
      panelClass: 'Snackbar2',
    });
  }
}
